Rails.application.routes.draw do
  resources :posts
  devise_for :users, controllers: { registrations: 'registrations' }
  as :user do
    get '/new_account', to: 'registrations#new'
  end
end
